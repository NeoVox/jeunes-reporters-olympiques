$tokenField = $('#tokenfield');

$tokenField.tokenfield({
  autocomplete: {
    source: $tokenField.data('url'),
    minLength: 2,
    delay: 100
  },
  showAutocompleteOnFocus: true
});


var textarea = document.querySelector('#editor')

if (window.tinyMCE) {

  tinymce.init({
    selector: '#editor',
    plugins: 'image,paste',
    paste_data_images: true,
    automatic_uploads: true,
    images_upload_handler: function (blobinfo, success, failure) {
      var data = new FormData()
      data.append('attachable_id', textarea.dataset.id)
      data.append('attachable_type', textarea.dataset.type)
      data.append('image', blobinfo.blob(), blobinfo.filename())
      axios.post(textarea.dataset.url, data)
          .then(function (res) {
            success(res.data.url)
          })
          .catch(function (err) {
            alert(err.response.statusText)
            success('http://placehold.it/350x150')
            // failure(err.response.statusText)
          })
    }
  })

}

if($('#editor').trumbowyg) {
  $('#editor').trumbowyg({
    btns: [
      ['formatting'],
      'btnGrp-design',
      ['link'],
      ['upload'],
      'btnGrp-justify',
      'btnGrp-lists',
      ['preformatted'],
      ['horizontalRule'],
      ['fullscreen']
    ],
    plugins: {
      upload: {
        serverPath: textarea.dataset.url,
        fileFieldName: 'image',
        urlPropertyName: 'url',
        statusPropertyName: 'id',
        data: [
          {name: 'attachable_type', value: textarea.dataset.type},
          {name: 'attachable_id', value: textarea.dataset.id}
        ],
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      }
    }
  })
}

if (window.CKEDITOR) {

  var editor = AlloyEditor.editable(textarea)
  editor.get('nativeEditor').on('imageAdd', function (event) {
    var data = new FormData()
    data.append('attachable_id', textarea.dataset.id)
    data.append('attachable_type', textarea.dataset.type)
    data.append('image', event.data.file)
    axios.post(textarea.dataset.url, data)
        .then(function (res) {
          event.data.el.$.src = res.data.url
        })
        .catch(function (err) {
          alert(err.response.statusText)
          event.data.el.$.remove()
        })
  })

}