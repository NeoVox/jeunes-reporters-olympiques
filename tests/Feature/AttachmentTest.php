<?php

namespace Tests\Feature;

use App\Attachment;
use App\Post;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AttachmentTest extends TestCase {

    public function setUp() {
        parent::setUp();
        Artisan::call('migrate');
        $this->cleanDirectories();
    }

    public function tearDown() {
        parent::tearDown();
        $this->cleanDirectories();
    }

    public function cleanDirectories () {
        Storage::disk('public')->deleteDirectory('uploads');
    }

    public function getFileForAttachment($attachment) {
        return dirname(__DIR__) . '/fixtures/uploads/' . $attachment['name'];
    }

    private function callController($data = []) {
        $path = dirname(__DIR__) . '/fixtures/demo.jpg';
        $file = new UploadedFile($path, 'demo.jpg', filesize($path), 'image/jpeg', null, true);
        $post = Post::create(['name' => 'demo', 'content' => 'demo']);
        $default = [
            'attachable_type' => Post::class,
            'attachable_id' => $post->id,
            'image' => $file
        ];
        return $this->post(route('attachments.store'), array_merge($default, $data));
    }

    public function testIncorrectDataAttachbleType () {
        $response = $this->callController(['attachable_type' => 'POOO']);
        $response->assertJsonStructure(['attachable_type']);
        $response->assertStatus(422);
    }

    public function testIncorrectDataAttachbleId () {
        $response = $this->callController(['attachable_id' => 3]);
        $response->assertJsonStructure(['attachable_id']);
        $response->assertStatus(422);
    }

    public function testCorrectData () {
        $response = $this->callController();
        $attachment = $response->json();
        $this->assertFileExists($this->getFileForAttachment($attachment));
        $response->assertJsonStructure(['id', 'url']);
        $this->assertContains('/uploads/', $attachment['url']);
        $response->assertStatus(200);
    }

    public function testDeleteAttachmentDeleteFile() {
        $response = $this->callController();
        $attachment = $response->json();
        $this->assertFileExists($this->getFileForAttachment($attachment));
        Attachment::find($attachment['id'])->delete();
        $this->assertFileNotExists($this->getFileForAttachment($attachment));
    }

    public function testDeletePostDeleteAllAttachments() {
        $response = $this->callController();
        $attachment = $response->json();
        factory(Attachment::class, 3)->create();
        $this->assertFileExists($this->getFileForAttachment($attachment));
        $this->assertEquals(4, Attachment::count());
        Post::first()->delete();
        $this->assertFileNotExists($this->getFileForAttachment($attachment));
        $this->assertEquals(3, Attachment::count());
    }

    public function testChangePostContentAttachmentsAreDeleted() {
        $response = $this->callController();
        $attachment = $response->json();
        factory(Attachment::class, 3)->create();
        $this->assertFileExists($this->getFileForAttachment($attachment));
        $this->assertEquals(4, Attachment::count());
        $post = Post::first();
        $post->content = "<img src=\"#{$attachment['url']}\"/> balbalbabla";
        $post->save();
        $this->assertEquals(4, Attachment::count());
        $post->content = "";
        $post->save();
        $this->assertEquals(3, Attachment::count());
        $this->assertFileNotExists($this->getFileForAttachment($attachment));
    }

    public function testChangePostContentAttachmentsAreDeletedIfImageChanged() {
        $response = $this->callController();
        $attachment = $response->json();
        factory(Attachment::class, 3)->create();
        $this->assertFileExists($this->getFileForAttachment($attachment));
        $this->assertEquals(4, Attachment::count());
        $post = Post::first();
        $post->content = "<img src=\"#{$attachment['url']}\"/> balbalbabla";
        $post->save();
        $this->assertEquals(4, Attachment::count());
        $post->content = "<img src=\"azeeaze/eazeazeazeaz/azeezaze.jpg\"/>";
        $post->save();
        $this->assertEquals(3, Attachment::count());
        $this->assertFileNotExists($this->getFileForAttachment($attachment));
    }

}