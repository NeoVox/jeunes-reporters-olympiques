<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/home', 'PagesController@home');

Auth::routes();

Route::post('/attachments', 'AttachmentController@store')->name('attachments.store');

Route::get('/tag/{slug}', 'TagController@tag')->name('posts.tag');

Route::get('/confirm/{id}/{token}', 'Auth\RegisterController@confirm');
Route::get('/profil', ['uses' => 'UsersController@edit', 'as' => 'profilEdit']);
Route::post('/profil', ['uses' => 'UsersController@update', 'as' => 'profilUpdate']);

Route::get('/user/{id}', 'UsersController@allById');

Route::post('/comments', 'CommentsController@store');
Route::get('/comments', 'CommentsController@index');
Route::delete('comments/{comment}', 'CommentsController@destroy');

Route::resource('/posts', 'PostsController');
Route::get('/posts/{slug}', ['uses' => 'PostsController@categories'])->where('slug', '[a-z\-]+');
Route::resource('/categories', 'CategoriesController');
Route::resource('/projects', 'ProjectsController');

Route::get('post/like/{id}', ['as' => 'post.like', 'uses' => 'LikeController@likePost']);

Route::get('/', 'HomeController@index');