<?php


namespace App\Concerns;


use App\Tag;
use Illuminate\Support\Str;

trait Taggable
{

    public static function bootTaggable() {
        static::observe(\App\Observer\TaggableObserver::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function saveTags(string $tags) {
        // Je récupère la liste des tags à associer à l'article
        $tags = array_filter(array_unique(array_map(function ($item) {
            return trim($item);
        }, explode(',', $tags))), function ($item) {
            return !empty($item);
        });

        // Je récupère les tags qui sont déjà en base de données
        $persisted_tags = Tag::whereIn('name', $tags)->get();

        // Je trouve les nouveaux tags, et je les insère en base
        $tags_to_create = array_diff($tags, $persisted_tags->pluck('name')->all());
        $tags_to_create = array_map(function ($tag) {
            return [
                'name' => $tag,
                'slug' => Str::slug($tag),
                'post_count' => 1
            ];
        }, $tags_to_create);
        $created_tags = $this->tags()->createMany($tags_to_create);
        $persisted_tags = $persisted_tags->merge($created_tags);
        $edits = $this->tags()->sync($persisted_tags);

        Tag::whereIn('id', $edits['attached'])->increment('post_count', 1);
        Tag::whereIn('id', $edits['detached'])->decrement('post_count', 1);
        Tag::removeUnused();
    }

    public function getTagsListAttribute(){
        return $this->tags->pluck('name')->implode(',');
    }
}