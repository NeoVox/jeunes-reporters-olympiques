<?php namespace App;

class Comments extends Model {

    public $guarded = ['id'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function posts(){
        return $this->belongsTo('App\Posts');
    }

    public function getUsernameAttribute($username){
        if($this->user_id){
            return $this->user->name;
        }
        return $username;
    }

    public function scopeCommentsFor($query, $user){
        return $query->join('posts', 'posts.id', '=', 'comments.posts_id')
            ->select('comments.*', 'posts.user_id as post_user_id')
            ->where('posts.user_id', $user->id);
    }

    public function scopeLastCommentsFor($query, $user, $count = 3){
        return $query->commentsFor($user)
            ->limit($count)
            ->get();
    }

    public function canEdit($user){
        if($user){
            if($user->id === $this->user_id){
                return true;
            } else if(
                ($this->post_user_id && $user->id === $this->post_user_id) ||
                ($user->id === $this->posts->user_id)
            ){
                return true;
            }
        }
        return false;
    }

}
