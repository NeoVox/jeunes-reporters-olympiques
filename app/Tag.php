<?php

namespace App;


class Tag extends Model
{

    public $guarded = [];
    public $timestamps = false;

    public function posts () {
        return $this->belongsToMany(Posts::class);
    }

    public static function removeUnused(){
        return static::where('post_count', 0)->delete();
    }

}
