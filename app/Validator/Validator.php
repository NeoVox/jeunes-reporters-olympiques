<?php

namespace App\Validator;

use App\Projects;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class Validator extends \Illuminate\Validation\Validator
{
    public function validateProjectsowner($attribute, $value, $parameters)
    {
        if(empty($value)){
            return false;
        }
        $projects_count = Projects::whereIn('id', $value)->where('user_id', Auth::user()->id)->count();
        return count($value) == $projects_count;
    }

    public function validateDimension($attribute, $value, $parameters)
    {
        $image = Image::make($value);
        return $image->width() >= $parameters[0] && $image->height() >= $parameters[1];
    }

    protected function replaceDimension($message, $attribute, $value, $parameters)
    {
        $message = str_replace(':width', $parameters[0], $message);
        $message = str_replace(':height', $parameters[1], $message);
        return $message;
    }
}