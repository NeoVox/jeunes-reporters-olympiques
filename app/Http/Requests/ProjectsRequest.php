<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "name" => "required|min:2",
            "type" => "required|in:A,RP,RV",
            "highlight" => "required|image",
            "categories_id" => "required|exists:categories,id"
        ];
        if($this->method() == 'PUT'){
            $rules['highlight'] = 'image';
        }
        return $rules;
    }
}
