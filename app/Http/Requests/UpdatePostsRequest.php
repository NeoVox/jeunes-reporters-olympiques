<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "name" => "required|min:3",
            "image" => "image|mimes:jpeg,png,jpg,gif|max:2048|dimensions:min_width=200,min_height=200"
            /*"image" => "image",
            "projects_id" => "required|projectsowner"*/
        ];
        if($this->method() == 'POST'){
            $rules['image'] .= '|required';
        }
        return $rules;
    }
}
