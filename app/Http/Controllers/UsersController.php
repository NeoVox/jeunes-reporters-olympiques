<?php

namespace App\Http\Controllers;

use App\Posts;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

class UsersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['allById']]);
    }

    /**
     * Edit the profile.
     */
    public function edit(Guard $auth)
    {
        $user = $auth->user();
        return view('users.edit', compact('user'));
    }

    /**
     * Update the profile.
     */
    public function update(Guard $auth, Request $request)
    {
        $user = $auth->user();
        $this->validate($request, [
            'name' => "required|unique:users,name,{$user->id}|min:2",
            'avatar' => "image"
        ]);        
        $user->update($request->only('name', 'firstname', 'lastname', 'avatar', 'type', 'about_me', 'ambitions'));
        return redirect()->back()->with('success','Votre profil a bien été modifié');
    }

    public function allById($id)
    {
        $posts = Posts::latest()->where('posts.user_id', "=", $id)->paginate(5);
        $user = User::findOrFail($id);
        return view('users.index', ['posts' => $posts, 'user' => $user]);
    }
}
