<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Http\Requests\CategoriesRequest;

class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * Show categories list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categories::all();
        return view('categories.index', compact('categories'));
    }

    /**
     * Create a categorie.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = new Categories();
        return view('categories.create', compact('categories'));
    }

    /**
     * Edit a categorie.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Categories::findOrFail($id);
        return view('categories.edit', compact('categories'));
    }

    /**
     * Save a categorie.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesRequest $request)
    {
        $categories = Categories::create($request->only('name', 'slug'));
        return redirect(action('CategoriesController@index'))->with('success', 'La catégorie a bien été créée');
    }

    /**
     * Update categorie informations.
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, CategoriesRequest $request)
    {
        $categories = Categories::findOrFail($id);
        $categories->update($request->only('name', 'slug'));
        return redirect(action('CategoriesController@index'))->with('success', 'La catégorie a bien été modifiée');
    }

    /**
     * Delete a categorie.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Categories::findOrFail($id);
        $categories->delete();
        return redirect(action('CategoriesController@index'))->with('success', 'La catégorie a bien été supprimée');
    }
}
