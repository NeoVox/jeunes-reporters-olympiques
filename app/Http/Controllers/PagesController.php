<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Projects;
use App\Tag;
use App\Posts;
use App\Categories;
use Illuminate\Http\Request;

class PagesController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home(){
        $posts = Posts::latest()->limit(8)->get();
        /*$categories = Categories::all();
        $projects = Projects::latest()->limit(4)->get();*/
        $tags = Tag::all();
        $max  = Tag::max('post_count');
        return view('pages.home', compact('posts', /*'categories', 'projects',*/ 'tags', 'max'));
    }

}
