<?php namespace App\Http\Controllers;

use App\Comments;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CommentsRequest;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class CommentsController extends Controller {

    public function __construct(){
        $this->middleware('auth', ['except' => ['store']]);
    }

    public function index(Guard $auth){
        $comments = Comments::commentsFor($auth->user())->with('posts')->paginate(10);
        return view('comments.user', compact('comments'));
    }

    public function store(CommentsRequest $request, Guard $auth){
        $data = $request->only('username', 'email', 'content', 'posts_id');
        if($auth->user()){
            $data['user_id'] = $auth->user()->id;
        }
        Comments::create($data);
        return redirect()->back()->with('success', 'Merci pour votre commentaire');
    }

    public function destroy($id, Guard $auth){
        $comments = Comments::findOrFail($id);
        if(!$comments->canEdit($auth->user())){
            return redirect()->back()->with('error', 'Vous ne pouvez pas �diter ce commentaire');
        }
        $comments->delete();
        return redirect()->back()->with('success', 'Le commentaire a bien �t� supprim�');
    }

}
