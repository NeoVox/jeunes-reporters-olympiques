<?php

namespace App\Http\Controllers;

use App\Projects;
use Illuminate\Http\Request;
use App\Http\Requests\ProjectsRequest;
use Illuminate\Contracts\Auth\Guard;

class ProjectsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
        $this->middleware('owner', ['except' => ['index', 'store' , 'create', 'show']]);
    }

    public function getResource($id)
    {
        return Projects::findOrFail($id);
    }

    /**
     * Show projects list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Guard $auth)
    {
        $projects = $auth->user()->projects;
        $projects->load('categories');
        return view('projects.index', compact('projects'));
    }

    /**
     * Create a project.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = new Projects();
        return view('projects.create', compact('projects'));
    }



    /**
     * Edit a project.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($projects)
    {
        return view('projects.edit', compact('projects'));
    }

    /**
     * Save a project.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectsRequest $request, Guard $auth)
    {
        $data = $request->all();
        $data['user_id'] = $auth->user()->id;
        Projects::create($data);
        return redirect(action('ProjectsController@index'))->with('success', 'Le projet a bien été créé');
    }

    /**
     * Update project informations.
     *
     * @return \Illuminate\Http\Response
     */
    public function update($projects, ProjectsRequest $request, Guard $auth)
    {
        $data = $request->all();
        $data['user_id'] = $auth->user()->id;
        $projects->update($data);
        return redirect(action('ProjectsController@index'))->with('success', 'Le projet a bien été modifié');
    }

    /**
     * Delete a project.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($projects)
    {
        $projects->delete();
        return redirect(action('ProjectsController@index'))->with('success', 'Le projet a bien été supprimé');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $projects = Projects::findOrFail($id);
        return view('projects.show', compact('projects'));
    }
}
