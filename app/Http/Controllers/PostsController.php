<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Posts;
use App\Tag;
use App\Categories;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\UpdatePostsRequest;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('owner', ['except' => ['index', 'store' , 'create', 'show', 'categories']]);
    }

    public function getResource($id)
    {
        return Posts::findOrFail($id);
    }

    public function categories($categories){
        $categories = Categories::where('slug', $categories)->firstOrFail();
        $posts = Posts::join('posts_projects', 'posts.id', '=', 'posts_projects.posts_id')
            ->join('projects', 'projects.id', '=', 'posts_projects.projects_id')
            ->where('projects.categories_id', $categories->id)
            ->selectRaw('posts.*')
            ->paginate(6);
        return view('posts.categories', compact('categories', 'posts'));
    }

    private function renderIndex($postQuery) {
        $posts = $postQuery->with('tags')->paginate(10);
        $tags = Tag::all();
        $max  = Tag::max('post_count');
        return view('posts.index', [
            'posts' => $posts,
            'tags' => $tags,
            'max' => $max
        ]);
    }

    /**
     * Create a post.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $posts = new Posts();
        return view('posts.create', ['posts' => $posts, 'editor' => 'tinymce']);
    }

    /**
     * Save a post.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UpdatePostsRequest $request, Guard $auth)
    {
        $data = $request->only('name', 'image', /*'projects_id',*/ 'content');
        $data['user_id'] = $auth->user()->id;
        $posts = Posts::create($data);
        $posts->saveTags($request->get('tags'));
        return redirect(action('PostsController@show', $posts))->with('success', "Le projet a bien été créé");
    }

    /**
     * Show a post.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Posts::findOrFail($id);
        $viewed = Session::get('viewed_post', []);
        if (!in_array($post->id, $viewed)) {
            $post->increment('view_count');
            Session::push('viewed_post', $post->id);
        }
        return view('posts.show', compact('post'));
    }

    /**
     * Show all posts.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Guard $auth)
    {
        return $this->renderIndex((new Posts())->newQuery());
        /*$posts = Posts::where('user_id', $auth->user()->id)->paginate(10);
        return view('posts.index', compact('posts'));*/
    }

    /**
     * Edit the post.
     */
    public function edit(Posts $posts)
    {
        return view('posts.edit', ['posts' => $posts, 'editor' => 'alloy']);
    }

    public function update($posts, UpdatePostsRequest $request)
    {
        $posts->update($request->all());
        $posts->saveTags($request->get('tags'));
        return redirect(action('PostsController@show', $posts))->with('success', "Le projet a bien été modifié");
    }

    public function destroy($post)
    {
        $post->delete();
        return redirect(action('PostsController@index'))->with('success', 'Le projet a bien été supprimé');
    }
}
