<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Posts;
use Illuminate\Http\Request;

class TagController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index (Request $request) {
        $term = $request->get('term');
        return Tag::select('name')
            ->where('name', 'LIKE', $term . '%')
            ->get()
            ->map(function ($tag) {
                return [
                    'id' => $tag->name,
                    'label' => $tag->name,
                    'value' => $tag->name
                ];
            });
    }

    public function tag ($slug) {
        $tag = Tag::where('slug', $slug)->first();
        $posts = $tag->posts()->with('tags')->paginate(10);
        return view('tag.show', [
            'tag' => $tag,
            'posts' => $posts
        ]);
    }
}
