<?php

namespace App;

use Intervention\Image\ImageManagerStatic;
use Illuminate\Support\Facades\Event;

class Projects extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    public $dates = [
        'created_at', 'updated_at',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleted(function($instance){
            if($instance->highlight){
                unlink(public_path() . $instance->highlight);
            }
            $instance->posts()->update(['projects_count' => DB::raw('projects_count - 1')]);
            foreach($instance->posts()->where('projects_count', 0)->get() as $post){
                Event::fire('eloquent.deleted: ' . get_class($post), $post);
            }
            $instance->posts()->where('projects_count', 0)->delete();
            return true;
        });
    }

    public function posts(){
        return $this->belongsToMany('App\Posts');
    }

    public function categories()
    {
        return $this->belongsTo('App\Categories');
    }

    public function getHighlightAttribute($highlight)
    {
        if($highlight){
            return "/img/highlight/" .ceil($this->id / 1000) . "/{$this->id}.jpg";
        }
        return false;
    }

    public function setHighlightAttribute($highlight)
    {
        if( is_object($highlight) && $highlight->isValid() ){
            self::saved(function($instance) use ($highlight){
                $dir = public_path() . "/img/highlight/" . ceil($instance->id / 1000);
                if(!file_exists($dir)){
                    mkdir($dir, 0777, true);
                }
                ImageManagerStatic::make($highlight)->fit(300,300)->save("$dir/{$instance->id}.jpg");
            });
            $this->attributes['highlight'] = true;
        }
    }

    public function getAgeAttribute()
    {
        return $this->created_at->diffForHumans();
    }
}
