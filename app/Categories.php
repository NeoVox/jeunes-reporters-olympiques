<?php

namespace App;

use App\Behavior\Sluggable;

class Categories extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug',
    ];

    //Behavior to generate a slug
    use Sluggable;
}
