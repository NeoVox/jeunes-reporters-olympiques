<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Intervention\Image\Facades\Image;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'confirmation_token', 'firstname', 'lastname', 'avatar', 'type', 'about_me', 'ambitions'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAvatarAttribute($avatar)
    {
        if($avatar){
            return "/img/avatars/{$this->id}.jpg";
        }
        return false;
    }

    public function setAvatarAttribute($avatar)
    {
        if( is_object($avatar) && $avatar->isValid() ){
            Image::make($avatar)->fit(150,150)->save(public_path() . "/img/avatars/{$this->id}.jpg");
            $this->attributes['avatar'] = true;
        }
    }

    public function projects()
    {
        return $this->hasMany('App\Projects');
    }

    public function posts()
    {
        return $this->hasMany('App\Posts');
    }

    public function comments(){
        return $this->hasMany('App\Comments');
    }

    public function commentsonposts(){
        return $this->hasManyThrough('App\Comments', 'App\Posts');
    }

    public function likedPosts()
    {
        return $this->morphedByMany('App\Posts', 'likeable')->whereDeletedAt(null);
    }
}
