<?php

namespace App;

use App\Concerns\AttachableConcern;
use App\Concerns\Taggable;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;

class Posts extends Model
{

    use AttachableConcern;
    use Taggable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'name', 'image', 'projects_id', 'content', 'user_id'
    ];

    public $formats = [
        'thumb' => [300,200],
        'large' => [940,530],
    ];

    public static function boot()
    {
        parent::boot();
        static::deleted(function($instance){
            $instance->detachImage();
            $instance->comments()->delete();
            return true;
        });
    }

    public function detachImage()
    {
        unlink($this->getImageDir() . "/{$this->id}.{$this->image}");
        foreach($this->formats as $format => $dimensions){
            unlink(public_path() . $this->image($format));
        }
    }

    public function image($size)
    {
        return '/' . $this->getImageDir() . '/' . $this->id . '_' . $size . '.jpg';
    }

    /*public function projects()
    {
        return $this->belongsToMany('App\Projects');
    }*/

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function comments(){
        return $this->hasMany('App\Comments');
    }

    public function getImageDir()
    {
        return 'img/photos/' . ceil ($this->id / 1000);
    }

    public function setImageAttribute($image)
    {
        if( is_object($image) && $image->isValid() ){
            /*if(!empty($this->image)){
                unlink($this->getImageDir() . "/{$this->id}.{$this->image}");
            }*/
            self::saved(function($instance) use ($image){
                $image = $image->move($instance->getImageDir(), $instance->id . '.' . $image->getClientOriginalExtension());
                foreach($instance->formats as $format => $dimensions){
                    Image::make($image)->fit($dimensions[0],$dimensions[1])->save(public_path() . $instance->image($format));
                }
            });
            $this->attributes['image'] = $image->getClientOriginalExtension();
        }
    }

    /*public function setProjectsIdAttribute($projects_id)
    {
        self::saved(function($instance) use ($projects_id){
            $instance->projects()->sync($projects_id);
        });
        $this->attributes['projects_count'] = count($projects_id);
    }

    public function getProjectsIdAttribute($projects_id)
    {
        if($this->id){
            return $this->projects->pluck('id');
        }
        return [];
    }*/

    public static function draft() {
        return self::firstOrCreate(['name' => null], ['content' => '']);
    }

    public function scopeNotDraft($query) {
        return $query->whereNotNull('name');
    }

    public function likes()
    {
        return $this->morphToMany('App\User', 'likeable')->whereDeletedAt(null);
    }

    public function getIsLikedAttribute()
    {
        $like = $this->likes()->whereUserId(Auth::id())->first();
        return (!is_null($like)) ? true : false;
    }
}
