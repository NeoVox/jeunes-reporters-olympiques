<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/style.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    @yield('head')

</head>
<body>
    <header class="header">
        <menu class="header-menu">
            <!-- MENU GAUCHE -->
            <nav class="header-menu-nav">
                <a href="{{ url('/home') }}">
                    <img src="{{ url('/') }}/img/assets/logo-menu.png" alt="Logo menu">
                </a>
                <a href="{{ url('/comments') }}" style="color:#FFFFFF; margin-right:0">
                    <span class="glyphicon glyphicon-comment"></span>
                </a>
                <!--span class="glyphicon glyphicon-bell"></span>
                <form id="search">
                    <input type="search" placeholder="Rechercher">
                </form-->
                <div class="header-menu-nav-link">
                    <a href="{{ url('/') }}">Accueil</a>
                    <a href="{{ url('/home') }}">Fil des Projets</a>
                    <a href="{{ url('/user') }}/{{ Auth::user()->id }}">Mon profil</a>
                    <span style="width: 20px">&nbsp;</span>
                    <a href="{{ url('/profil') }}">{{ Auth::user()->name }} - Mon compte</a>
                    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    D&eacute;connexion
                    </a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">  {{ csrf_field() }}
                    </form>
                </div>
            </nav>
            <!-- MENU DROIT -->
            <a class="header-menu-icon" href="#"><img src="{{ url('/') }}/img/assets/menu.svg" alt="Menu vertical mobile"></a>
        </menu>
        <!-- SCRIPT RESPONSIVE MENU -->
        <script src="js/menu.js"></script>
    </header>

        @if(session('success'))
            <div class="container">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        @endif

        @if(session('error'))
            <div class="container">
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            </div>
        @endif

        @yield('content')

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script src="{{ url('/js/laravel.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    @yield('js')
</body>
</html>
