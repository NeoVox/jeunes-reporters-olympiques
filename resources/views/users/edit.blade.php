@extends('layouts.sidebar')

@section('main')
    <main class="main">
        <!-- UPDATE ACCOUNT -->
        <section class="main-account">
            <div class="main-articles-menu">
                <div class="main-articles-menu-nav">
                    <h4>Votre compte utilisateur</h4>
                </div>
            </div>
            <div class="main-account-update">

                {!! Form::model($user, ['class' => '', 'files' => true ]) !!}

                    <label for="avatar">Avatar</label>

                    <div class="col-md-6">
                        @if($user->avatar)
                            <img src="{{ url ($user->avatar) }}" />
                        @endif
                        {!! Form::file('avatar') !!}

                        @if ($errors->has('avatar'))
                            <span class="help-block">
                                <strong>{{ $errors->first('avatar') }}</strong>
                            </span>
                        @endif
                    </div>

                    <p>&nbsp;</p>

                    <label for="name">Pseudo</label>

                    {!! Form::text('name', null, ['class' => 'b-input form-control']) !!}

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif


                    <label for="email">Email</label>

                    {!! Form::email('email', null, ['class' => 'b-input form-control', 'disabled']) !!}

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif


                    <label for="firstname">Prénom</label>

                    {!! Form::text('firstname', null, ['class' => 'b-input form-control']) !!}

                    @if ($errors->has('firstname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('firstname') }}</strong>
                        </span>
                    @endif


                    <label for="lastname">Nom</label>

                    {!! Form::text('lastname', null, ['class' => 'b-input form-control']) !!}

                    @if ($errors->has('lastname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lastname') }}</strong>
                        </span>
                    @endif

                    <label for="type">Vous êtes</label>
                    {{ Form::select('type', [
                        'null'      => 'Plusieurs choix disponibles...',
                        'etudiant'  => 'Étudiant',
                        'prof'      => 'Professeur',
                        'entreprise' => 'Entreprise'
                    ], old('type'), ['class' => 'b-type form-control'] ) }}

                    <label for="about_me">À propos de moi</label>
                    {!! Form::textarea('about_me', null, ['class' => 'b-textarea form-control', 'rows' => '3']) !!}

                    <label for="ambitions">Mes ambitions</label>
                    {!! Form::textarea('ambitions', null, ['class' => 'b-textarea form-control', 'rows' => '3']) !!}

                    <button type="submit" class="b-submit btn btn-primary">
                        Modifier
                    </button>

                {!! Form::close() !!}

            </div>
        </section>
    </main>

@endsection

@section('main')
    @include('users.sidebar', ['tab' => 'profil'] )
@endsection