@extends('layouts.sidebar')

@section('sidebar')
    @if($tab != 'comments')
        <h4>Derniers commentaires</h4>
        @include('comments.index', ['comments' => App\Comments::lastCommentsFor(Auth::user())])
        <p class="text-right">
            <a class="btn btn-primary" href="{{ action('CommentsController@index') }}">Voir tous les commentaires</a>
        </p>
    @endif

    <div class="list-group">
        <a href="{{ action('PostsController@create') }}" class="list-group-item {{ $tab == "posts" ? "active" : "" }}">Ajouter une photo</a>
        <a href="{{ action('PostsController@index') }}" class="list-group-item {{ $tab == "postsList" ? "active" : "" }}">Mes photos</a>
        <a href="{{ action('ProjectsController@index') }}" class="list-group-item {{ $tab == "projects" ? "active" : "" }}">Mes projets</a>
        <a href="{{ url('profil') }}" class="list-group-item {{ $tab == "profil" ? "active" : "" }}">Mon profil</a>
    </div>
@endsection
