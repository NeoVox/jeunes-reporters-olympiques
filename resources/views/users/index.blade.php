@extends('layouts.app')

@section('content')
<div class="back"></div>

<!-- CONTENU PRINCIPAL -->
<main class="main">
    <!-- PROFIL -->
    <section class="main-profil">
        @if($user->avatar)
            <div style="text-align:center">
                <img class="photo" src="{{ url ($user->avatar) }}"  alt="Photo de {{ $user->firstname }} {{ $user->lastname }}"/>
            </div>
        @endif
        <p class="name" style="line-height: 1">{{ $user->firstname }} {{ $user->lastname }}
            @if(!empty($user->type) && $user->type != 'null')
            <br><i style="font-size:0.5em; color:grey">({{ $user->type }})</i>
        @endif
        </p>
        <!--p class="info">
            <span><span class="glyphicon glyphicon-eye-open"></span> NOMBRE DE VUES PROJETS</span>
            <span class="number">2367</span>
        </p-->
        <!--p class="info">
            <span><span class="glyphicon glyphicon-thumbs-up"></span> APPR&Eacute;CIATIONS</span>
            <span class="number">1892</span>
        </p-->
        <div class="about">
            <h3 class="title">&Agrave; propos de moi</h3>
            <p class="txt">{{ $user->about_me }}</p>
        </div>
        <div class="ambition">
            <h3 class="title">Mes ambitions</h3>
            <p class="txt">{{ $user->ambitions }}</p>
        </div>
    </section>
    <!-- ARTICLES -->

    <section class="main-articles">
        <div class="main-articles-menu">
            <div class="main-articles-menu-nav">
                <h4>Tous les projets</h4>
                <!--a href="#">Les plus vus</a>
                <a href="#">Les plus r&eacute;cents</a-->
            </div>
            <a href="{{ action('PostsController@create') }}"><button>Cr&eacute;er un article</button></a>
        </div>
        @foreach($posts as $post)
        <article class="article">
            <h3 class="title"><a href="{{ action('PostsController@show', $post) }}">{{ $post->name }}</a></h3>
            <p class="by">Film r&eacute;alis&eacute; par {{ $post->user()->value('firstname') }} {{ $post->user()->value('lastname') }}</p>
            <a href="{{ action('PostsController@show', $post) }}"><img class="photo" src="{{ url($post->image('large')) }}" alt="Photo du reportage"></a>
            <div class="subscription">
                <!--button class="btn">
                    <span class="star glyphicon glyphicon-star"></span>
                    S'abonner
                    <span class="number">134</span>
                </button-->
                @if ($post->isLiked)
                    <p class="like">
                        <a href="{{ route('post.like', $post->id) }}" style="color:grey">
                            <span class="glyphicon glyphicon-thumbs-down">
                                Dislike
                            </span>
                        </a>
                    </p>
                @else
                    <p class="like">
                        <a href="{{ route('post.like', $post->id) }}" style="color:grey">
                            <span class="glyphicon glyphicon-thumbs-up">
                                Like
                            </span>
                        </a>
                    </p>
                @endif
                <p class="view"><span class="glyphicon glyphicon-eye-open"></span>{{ $post->view_count }}</p>
            </div>
            <h5 class="date">Ajout&eacute; le {{ date('d-m-Y', strtotime($post->created_at)) }}</h5>
            <p class="description">{{ substr(strip_tags($post->content), 0, 140) }}</p>
            <div class="comment">
                @include('comments.form')
                @if(count($post->comments) != 0)
                    <p class="nb-comments"><span class="glyphicon glyphicon-play"></span>{{ count($post->comments) }}</p>
                    <p class="look"><a href="{{ action('PostsController@show', $post) }}">Voir les commentaires</a></p>
                @endif
                {{--@include('comments.index', ['comments' => $post->comments])--}}

            </div>
        </article>
        @endforeach
        <div class="pagination">
            {!! $posts->render() !!}
        </div>
    </section>
</main>

@endsection