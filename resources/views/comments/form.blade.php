{!! Form::open(['url' => action('CommentsController@store'), 'class' => 'form']) !!}

<!--div class="row"-->
    {{--@if(Auth::guest())--}}

        <!--div class="col-md-6">

            <div class="form-group{{-- $errors->has('username') ? ' has-error' : '' --}}">

                {--!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Pseudo']) !!--}

                {{--@if ($errors->has('username'))--}}
                    <span class="help-block">
                        <strong>{{-- $errors->first('username') --}}</strong>
                    </span>
                {{--@endif--}}
            </div>
        </div>

        <div class="col-md-6">

            <div class="form-group{{-- $errors->has('email') ? ' has-error' : '' --}}">

                {--!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!--}

                {{--@if ($errors->has('email'))--}}
                    <span class="help-block">
                        <strong>{{-- $errors->first('email') --}}</strong>
                    </span>
                {{--@endif--}}
            </div>
        </div-->

    {{--@endif--}}

        {!! Form::textarea('content', null, ['class' => 'text', 'placeholder' => 'Votre commentaire', 'rows' => '2']) !!}

        @if ($errors->has('content'))
            <span class="help-block">
                <strong>{{ $errors->first('content') }}</strong>
            </span>
        @endif

        {!! Form::hidden('posts_id', $post->id) !!}
<!--/div-->

        <button class="submit">Envoyer</button>

        {!! Form::close() !!}