@extends('layouts.sidebar')

@section('active', 'profil')

@section('main')
    <h2>Les commentaires sur mes projets</h2>
    @include('comments.index', ['posts' => true])
@endsection

@section('sidebar')
    @include('users.sidebar', ['tab' => 'comments'])
@endsection