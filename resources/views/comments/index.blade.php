<?php $comments->load('user') ?>
@foreach($comments as $comment)
    <p>
        <strong>{{ $comment->username }}</strong>, <em>{{ $comment->created_at->diffForHumans() }}</em>
        @if(isset($posts) && $posts === true)
            <br><em><a href="{{ action('PostsController@show', $comment->posts) }}">{{ $comment->posts->name }}</a></em>
        @endif
        <br>
        {{ $comment->content }}
    @if($comment->canEdit(Auth::user()))
        <a href="{{ action('CommentsController@destroy', $comment) }}" data-method="DELETE" data-confirm="Voulez vous vraiment supprimer ce commentaire" style="color: darkred; margin-left:15px">X</a>
    @endif
    </p>
@endforeach
