@extends('layouts.sidebar')

@section('main')
    <h2>Derniers projets</h2>
    @foreach($tags as $tag)
        <a style="font-size:{{ 1 + round($tag->post_count / $max, 2) }}rem; color:grey" href="{{ route('posts.tag', ['slug' => $tag->slug]) }}">#{{ $tag->name }}</a>
    @endforeach
    <main class="arts">
    <!--div class="row"-->
        @foreach($posts as $post)
            <article class="arts-article">
                <div class="arts-article-thumb">
                    <img class="arts-article-thumb-picture" src="{{ url($post->image('thumb')) }}" alt="">
                </div>
                <div class="arts-article-preview">
                    <div class="arts-article-preview-window">
                        <img class="arts-article-preview-window-avatar" src="img/avatars/{{ $post->user()->value('id') }}.jpg" alt="Auteur de l'article" height="100">
                        <p class="arts-article-preview-window-author">{{ $post->user()->value('firstname') }}<br>{{ $post->user()->value('lastname') }}</p>
                    </div>
                    <h4 class="arts-article-preview-title"><a href="{{ action('PostsController@show', $post) }}">{{ $post->name }}</a></h4>
                    <p class="arts-article-preview-txt">{{ substr(strip_tags($post->content), 0, 140) }}</p>
                    <div class="arts-article-preview-info">
                        <p class="arts-article-preview-info-spec">
                            <a href="{{ action('PostsController@show', $post) }}">Voir le projet</a>
                            <!--span class="glyphicon glyphicon-eye-open"></span><span class="number">2367</span>
                            <span class="glyphicon glyphicon-thumbs-up"></span><span class="number">1892</span-->
                        </p>
                        <p class="arts-article-preview-info-date">{{ substr($post->created_at, 0, 10) }}</p>
                    </div>
                </div>
            </article>
        @endforeach

    <!--p class="text-right">
        <a class="btn btn-primary" href="{{-- action('PostsController@index') --}}">Voir toutes les photos</a>
    </p-->
    </main>
@endsection



{{--@section('sidebar')--}}
    <!--h2>Filtrer </h2>
    <div class="list-group">
        {{--@foreach($categories as $s)--}}
            <a class="list-group-item" href="{{-- action('PostsController@categories', $s->slug) --}}">
                {{-- $s->name --}}
            </a>
        {{--@endforeach--}}
    </div>

    <h2>Nouveaux projets </h2>
    <div class="list-group">
        {{--@foreach($projects as $project)--}}
            <div class="list-group-item">
                <h4 class="list-group-item-heading">{{-- $project->name --}}</h4>
                <p class="list-group-item-text">{{-- $project->age --}} ans</p>
                <a href="{{-- action('ProjectsController@show', $project) --}}">Voir le projet</a>
            </div>
        {{--@endforeach--}}
    </div>-->
{{--@endsection--}}