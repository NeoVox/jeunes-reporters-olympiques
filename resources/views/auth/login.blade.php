@extends('layouts.free')

@section('content')
<section class="section1">
    <header class="section1-header">
        <img class="section1-header-logo" src="img/assets/logo-a-waree.png" alt="logo a-waree">
        <div class="section1-header-register">
            <form class="section1-header-register-form" role="form" method="POST" action="{{ url('/login') }}">

                {{ csrf_field() }}

                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif

                <input id="password" type="password" class="form-control" name="password" placeholder="Mot de passe" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

                <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}>
                    <span class="label-txt">Rester connect&eacute;</span>
                </label>

                <input type="submit" value="Connexion">

                <a href="{{ url('/password/reset') }}">
                    Mot de passe oubli&eacute; ?
                </a>
                <a href="{{ url('/register') }}">
                    S'inscrire
                </a>

            </form>
        </div>
    </header>
</section>
@endsection
