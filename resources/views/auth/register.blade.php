@extends('layouts.free')

@section('content')
<section class="section1">
    <div class="section1-header">
        <img class="section1-header-logo" src="img/assets/logo-a-waree.png" alt="logo a-waree">
        <div class="section1-header-register">
            <form class="section1-header-register-form" role="form" method="POST" action="{{ url('/register') }}">

                {{ csrf_field() }}

                <input id="name" type="text" name="name" value="{{ old('name') }}" placeholder="Pseudo" required autofocus>

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif

                <input id="email" type="email" name="email" value="{{ old('email') }}" placeholder="Adresse email" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif

                <input id="password" type="password" name="password" placeholder="Cr&eacute;ez un mot de passe" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

                <input id="password-confirm" type="password" name="password_confirmation" placeholder="Entrez de nouveau votre mot de passe" required>

                <label>
                    <input type="checkbox" name="check" value="connect" required>
                    <span class="label-txt">J'accepte les <a href="cgu.html" data-featherlight="cgu.html">conditions g&eacute;n&eacute;rales d'utilisation</a></span>
                </label>

                <input type="submit" value="Inscription">
            </form>

        </div>
    </div>
</section>
@endsection

@section('head')
    <link href="//cdn.rawgit.com/noelboss/featherlight/1.7.12/release/featherlight.min.css" type="text/css" rel="stylesheet" />
@endsection
@section('js')
    <script src="//cdn.rawgit.com/noelboss/featherlight/1.7.12/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
@endsection