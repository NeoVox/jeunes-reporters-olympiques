@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{ $post->name }}</h1>
                @if(Auth::user() && Auth::user()->id == $post->user_id)
                    <p class="text-center">
                        <a href="{{ action('PostsController@edit', $post ) }}" class="btn btn-primary">Editer</a>
                        <a class="btn btn-danger" href="{{ action('PostsController@destroy', $post) }}" data-method="delete" data-confirm="Voulez vous vraiment supprimer cet enregistrement">Supprimer</a>
                    </p>
                @endif
            </div>
            
            <p class="text-center">
                <img src="{{ url($post->image('large')) }}" alt="" width="100%"/>
            </p>

            <p style="font-size:12px">
                {!! $post->content !!}
            </p>

            <hr>

            <div class="row">
                <div class="col-md-12">
                    <h4>Tags</h4>
                    <div class="list-group">
                        @foreach($post->tags as $tag)
                            <a href="{{ route('posts.tag', ['slug' => $tag->slug]) }}" class="badge badge-default">{{ $tag->name }}</a>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-12">
                    <h3>Commentaires</h3>
                    @include('comments.index', ['comments' => $post->comments])
                    @include('comments.form')
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection