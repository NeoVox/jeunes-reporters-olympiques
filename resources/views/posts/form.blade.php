{!! Form::model($posts, ['class' => 'form-horizontal', 'url' => action("PostsController@$action", $posts), 'method' => $action == "store" ? "Post" : "Put", 'files' => true ]) !!}

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label">Nom</label>

    <div class="col-md-12">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    <label for="image" class="col-md-4 control-label">Image</label>

    <div class="col-md-12">

        {!! Form::file('image', ['class' => 'form-control']) !!}

        @if ($errors->has('image'))
            <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
    <label for="content" class="col-md-4 control-label">Description</label>

    <div class="col-md-12">
        <textarea data-id="{{ $posts->id }}" data-type="{{ get_class($posts) }}" data-url="{{ route('attachments.store') }}" name="content" id="editor" class="form-control">{{ $posts->content }}</textarea>

        @if ($errors->has('content'))
            <span class="help-block">
                <strong>{{ $errors->first('content') }}</strong>
            </span>
        @endif
    </div>
</div>

<!--div class="form-group{{--{{ $errors->has('projects_id') ? ' has-error' : '' }}--}}">
    <label for="projects_id" class="col-md-4 control-label">Projet</label>

    <div class="col-md-12">
        {{--{!! Form::select('projects_id[]', Auth::user()->projects->pluck('name', 'id'), null, ['placeholder' => 'Choisissez un projet...', 'class' => 'form-control', 'multiple']) !!}--}}

        {{--@if ($errors->has('projects_id'))--}}
            <span class="help-block">
                <strong>{{--{{ $errors->first('projects_id') }}--}}</strong>
            </span>
        {{--@endif--}}
    </div>
</div-->

<div class="form-group">
    <label class="col-md-4 control-label">Tags</label>

    <div class="col-md-12">
        <input data-url="{{ route('tags.index') }}" id="tokenfield" type="text" name="tags" class="form-control" value="{{ old('tags', $posts->tagsList) }}" placeholder="tags">
    </div>
</div>


<div class="form-group">
    <div class="col-md-8 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            @if($action == 'store')
                Ajouter un projet
            @else
                Éditer ce projet
            @endif
        </button>
    </div>
</div>

{!! Form::close() !!}

@section('head')
    <link rel="stylesheet" href="/css/bootstrap-tokenfield.min.css">
    <link rel="stylesheet" href="/js/trumbowyg/ui/trumbowyg.min.css">
@endsection
@section('js')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script
            src="http://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
            integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
            crossorigin="anonymous"></script>
    <script src="/js/bootstrap-tokenfield.min.js"></script>
    <script src="/js/trumbowyg/trumbowyg.min.js"></script>
    <script src="/js/trumbowyg/plugins/upload/trumbowyg.upload.min.js"></script>
    <script src="/js/editor.js"></script>
@endsection

@if($editor == 'tinymce')
@section('js')
    <script src="//cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="/js/editor.js"></script>
@endsection
@elseif($editor == 'trumbowyg')
@section('js')

    <script src="/js/editor.js"></script>
@endsection

@section('head')

@endsection

@elseif($editor == 'alloy')
@section('js')
    <script src="/js/alloy/alloy-editor-all.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="/js/editor.js"></script>
@endsection

@section('head')
    <link rel="stylesheet" href="/js/alloy/assets/alloy-editor-atlas-min.css">
@endsection
@endif