@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Toutes les photos de {{ $categories->name }}</h1>

    <div class="row">
        @foreach($posts as $post)
            <div class="col-sm-4">
                @include('posts.single')
            </div>
        @endforeach
    </div>

    <div class="pagination">
        {!! $posts->render() !!}
    </div>
</div>
@endsection