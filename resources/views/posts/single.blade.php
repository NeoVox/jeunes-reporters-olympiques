<a href="{{ action('PostsController@show', $post) }}">
    <h4>{{ $post->name }}</h4>
    <img src="{{ url($post->image('thumb')) }}" alt="" />
</a>