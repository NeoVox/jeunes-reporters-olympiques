@extends('layouts.sidebar')

@section('main')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <h1>Mes photos</h1>

            <div class="row">
            @foreach($posts as $post)
                <div class="col-md-6">
                    @include('posts.single')
                </div>
            @endforeach
            </div>
            <br>
            <div class="pagination">
                {!! $posts->render() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('main')
    @include('users.sidebar', ['tab' => 'postsList'] )
@endsection