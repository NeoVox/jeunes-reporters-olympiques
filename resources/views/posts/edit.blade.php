@extends('layouts.sidebar')

@section('main')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>Modifier la photo {{ $posts->name }}</h1>

                @include('posts.form', ['action' => 'update'])

            </div>
        </div>
    </div>
@endsection

@section('main')
    @include('users.sidebar', ['tab' => 'posts'] )
@endsection