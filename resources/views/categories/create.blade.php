@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Ajouter une nouvelle catégorie</h1>
            
            @include('categories.form', ['action' => 'store'])
            
        </div>
    </div>
</div>
@endsection