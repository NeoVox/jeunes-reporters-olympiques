@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Modifier la catégorie {{ $categories->name }}</h1>
            
            @include('categories.form', ['action' => 'update'])
            
        </div>
    </div>
</div>
@endsection