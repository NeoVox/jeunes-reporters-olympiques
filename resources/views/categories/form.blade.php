{!! Form::model($categories, ['class' => 'form-horizontal', 'url' => action("CategoriesController@$action", $categories), 'method' => $action == "store" ? "Post" : "Put"]) !!}

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label">Nom de la catégorie</label>

    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
    <label for="slug" class="col-md-4 control-label">URL de la catégorie</label>

    <div class="col-md-6">
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}

        @if ($errors->has('slug'))
            <span class="help-block">
                <strong>{{ $errors->first('slug') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-8 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            Enregistrer
        </button>
    </div>
</div>

{!! Form::close() !!}