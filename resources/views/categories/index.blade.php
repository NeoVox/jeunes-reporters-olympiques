@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Gérer les catégories</h1>
            
            <p class="text-right">
                <a href="{{ action('CategoriesController@create') }}" class="btn btn-primary">
                    Ajouter une catégorie
                </a>
            </p>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nom</th>
                        <th>Slug</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($categories as $categorie)
                    <tr>
                        <td>{{ $categorie->id }}</td>
                        <td>{{ $categorie->name }}</td>
                        <td>{{ $categorie->slug }}</td>
                        <td>
                            <a href="{{ action('CategoriesController@edit', $categorie) }}" class="btn btn-primary">
                                Editer
                            </a>
                            <a href="{{ action('CategoriesController@destroy', $categorie) }}" class="btn btn-danger" data-method="delete" data-confirm="Voulez-vous vraiment supprimer cette catégorie ?">
                                Supprimer
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            
        </div>
    </div>
</div>
@endsection