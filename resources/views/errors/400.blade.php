@extends('layouts.app')

@section('content')
    <section class="back404">
        <div class="back404-txt">
            <h1 class="back404-txt-title">OUPS !</h1>
            <h3 class="back404-txt-st">Tu n'es pas au bon endroit !</h3>
            <a class="back404-txt-link" href="{{ url('/') }}">Retourner au point de d�part</a>
        </div>
    </section>
@endsection