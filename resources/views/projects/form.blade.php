{!! Form::model($projects, ['class' => 'form-horizontal', 'url' => action("ProjectsController@$action", $projects), 'method' => $action == "store" ? "Post" : "Put", 'files' => true ]) !!}

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label">Nom du projet</label>

    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
    <label for="type" class="col-md-4 control-label">Type de projet</label>

    <div class="col-md-6">
        {!! Form::select('type', ['A' => 'Article', 'RP' => 'Reportage Photo', 'RV' => 'Reportage Vidéo'], null, ['placeholder' => 'Choisissez un type...', 'class' => 'form-control']) !!}

        @if ($errors->has('type'))
            <span class="help-block">
                <strong>{{ $errors->first('type') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('highlight') ? ' has-error' : '' }}">
    <label for="highlight" class="col-md-4 control-label">Image à la Une</label>

    <div class="col-md-6">
        @if($projects->highlight)
            <img src="{{ url ($projects->highlight) }}" />
        @endif
        {!! Form::file('highlight', ['class' => 'form-control']) !!}

        @if ($errors->has('highlight'))
            <span class="help-block">
                <strong>{{ $errors->first('highlight') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('categories_id') ? ' has-error' : '' }}">
    <label for="categories_id" class="col-md-4 control-label">Catégorie</label>

    <div class="col-md-6">
        {!! Form::select('categories_id', App\Categories::pluck('name', 'id'), null, ['placeholder' => 'Choisissez une catégorie...', 'class' => 'form-control']) !!}

        @if ($errors->has('categories_id'))
            <span class="help-block">
                <strong>{{ $errors->first('categories_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-8 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            Enregistrer
        </button>
    </div>
</div>

{!! Form::close() !!}