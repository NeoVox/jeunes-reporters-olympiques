@extends('layouts.sidebar')

@section('main')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <h1>Ajouter un nouveau projet</h1>
            
            @include('projects.form', ['action' => 'store'])
            
        </div>
    </div>
</div>
@endsection

@section('main')
    @include('users.sidebar', ['tab' => 'projects'] )
@endsection