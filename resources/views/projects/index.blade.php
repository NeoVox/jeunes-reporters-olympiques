@extends('layouts.sidebar')

@section('main')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <h1>Gérer les projets</h1>
            
            <p class="text-right">
                <a href="{{ action('ProjectsController@create') }}" class="btn btn-primary">
                    Ajouter un projet
                </a>
            </p>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Nom</th>
                        <th>Type</th>
                        <th>Il y a</th>
                        <th>Catégorie</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($projects as $project)
                    <tr>
                        <td><img src="{{ url($project->highlight) }}?{{ time() }}" width="50" class="img-circle" /></td>
                        <td>{{ $project->name }}</td>
                        <td>{{ $project->type }}</td>
                        <td>{{ $project->age }}</td>
                        <td>{{ $project->categories->name }}</td>
                        <td>
                            <a class="btn btn-default" href="{{ action('ProjectsController@show', $project) }}">Voir</a>
                            <a href="{{ action('ProjectsController@edit', $project) }}" class="btn btn-primary">
                                Editer
                            </a>
                            <a href="{{ action('ProjectsController@destroy', $project) }}" class="btn btn-danger" data-method="delete" data-confirm="Voulez-vous vraiment supprimer ce projet ?">
                                Supprimer
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            
        </div>
    </div>
</div>
@endsection

@section('main')
    @include('users.sidebar', ['tab' => 'projects'] )
@endsection