@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <img src="{{ url($projects->highlight) }}" class="img-circle" width="150"/>
        </div>
        <div class="col-md-10">
            <h2>{{ $projects->name }}</h2>
            <p>
                {{ $projects->categories->name }}, {{ $projects->age }}
            </p>
        </div>
    </div>
    <div class="row">
        @foreach($posts = $projects->posts()->paginate(10) as $post)
            <div class="col-md-4">
                @include('posts.single')
            </div>
        @endforeach
    </div>
    <div class="pagination">
        {!! $posts->render() !!}
    </div>
</div>
@endsection