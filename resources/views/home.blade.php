@extends('layouts.free')

@section('content')
    <section class="section1" id="accueil">
        <header class="section1-header">
            <img class="section1-header-logo" src="img/assets/logo-a-waree.png" alt="logo a-waree">
            <div class="section1-header-txt">
                <h1 class="section1-header-txt-title">Les jeunes Reporters Olympiques</h1>
                <h3 class="section1-header-txt-subtitle">jeunesse, cr&eacute;ativit&eacute;, sport</h3>
                <div class="section1-header-txt-btn">
                    <a href="{{ url('/login') }}">Connexion</a>
                    <a href="{{ url('/register') }}">Inscription</a>
                </div>
            </div>
            <a class="section1-header-redirection" href="#section2"><img src="img/assets/down.png" alt="visualiser les informations"></a>
        </header>
    </section>

    <section class="section2" id="section2">
        <img class="section2-sport" src="img/assets/projet-jeunes-reporters.png" alt="Projet Jeunes Reporters Olympique">
        <div class="section2-projet">
            <div class="section2-projet-col1">
                <img class="section2-projet-col1-student" src="img/assets/etudiant-reporters.jpg" alt="Etudiant reporter">
                <div class="section2-projet-col1-color"></div>
            </div>
            <article class="section2-projet-col2">
                <h2>Le projet : les jeunes et les jeux olympiques</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
            </article>
        </div>
    </section>

    <section class="section3">
        <h2 class="section3-title">Pour qui ?</h2>
        <div class="section3-articles">
            <div class="section3-articles-art">
                <div class="section3-articles-art-picture student">
                    <div class="hidden"></div>
                    <h2>&Eacute;tudiants</h2>
                    <button></button>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
            </div>
            <div class="section3-articles-art">
                <p class="txt-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididuntenim.</p>
                <div class="section3-articles-art-picture school">
                    <div class="hidden"></div>
                    <h2>&Eacute;coles/<br>Institutions</h2>
                    <button></button>
                </div>
            </div>
        </div>
    </section>

    <section class="section4">
        <div class="section4-how">
            <article class="section4-how-art">
                <h2>Comment &ccedil;a marche ?</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididuntenim .do consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
            </article>
            <img src="img/assets/explications-demarches.png" alt="Comment �a marche ? explications !">
        </div>
    </section>

    <section class="section5">
        <h2>D&eacute;couvrir les projets &eacute;tudiants</h2>
        <div class="carousel">
            <a class="carousel-item" href="#"><img src="https://lorempixel.com/250/250/nature/1"></a>
            <a class="carousel-item" href="#"><img src="https://lorempixel.com/250/250/nature/2"></a>
            <a class="carousel-item" href="#"><img src="https://lorempixel.com/250/250/nature/3"></a>
            <a class="carousel-item" href="#"><img src="https://lorempixel.com/250/250/nature/4"></a>
            <a class="carousel-item" href="#"><img src="https://lorempixel.com/250/250/nature/5"></a>
        </div>
        <!-- Script for carousel // http://materializecss.com -->
        <script>
            $(document).ready(function(){
                $('.carousel').carousel();
            });
        </script>
        <a href="articles.php"><button type="button" name="button">Fil d'actualit&eacute; des projets</button></a>
    </section>

    <section class="section6">
        <h2>Suis le projet des Reporters Olympiques</h2>
        <p>Rejoins la Newsletter des reporters olympiques pour �tre inform&eacute; de l'avancement des projets</p>
        <form class="section6-form" action="index.html" method="post">
            <input id="input" type="email" name="email" value="" placeholder="Adresse email">
            <input id="input" type="text" name="" value="" placeholder="Nom, Pr&eacute;nom">
            <!-- Bouton ON-OFF retir&eacute; -->
            <!-- <div class="news">
              <label class="switch">
                <input type="checkbox">
                <span id="slider" class="round"></span>
              </label>
              <p>E-news ?</p>
            </div> -->
            <input type="submit" name="" value="Join">
        </form>
        <div class="section6-inscription">
            <img class="section6-inscription-deco deco1" src="img/assets/decoration.png" alt="D&eacute;coration">
            <img class="section6-inscription-deco deco2" src="img/assets/decoration.png" alt="D&eacute;coration">
            <img class="section6-inscription-deco deco3" src="img/assets/decoration.png" alt="D&eacute;coration">
            <img class="section6-inscription-deco deco4" src="img/assets/decoration.png" alt="D&eacute;coration">
            <a href="{{ url('/login') }}"><button type="button" name="button">Rejoins l'aventure</button></a>
        </div>
        <a class="section6-up" href="#accueil"><img src="img/assets/up.jpg" alt="Scroll vers le haut"></a>
    </section>

    <!-- Scrollbar Script -->
    <script src="js/scroll.js"></script>
@endsection

@section('head')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
@endsection