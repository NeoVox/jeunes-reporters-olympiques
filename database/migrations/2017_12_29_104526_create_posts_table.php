<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('content');
            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::create('posts_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('posts_id')->unsigned()->index();
            $table->integer('projects_id')->unsigned()->index();
            $table->foreign('projects_id')->references('id')->on('posts')->onDelete('cascade');
            $table->foreign('posts_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
