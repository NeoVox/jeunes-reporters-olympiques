![Logo Laravel](https://laravel.com/assets/img/components/logo-laravel.svg)

# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

## Laravel : Installation sur une machine locale

Le clonage d'un dépôt necessite l'installation de Git (Linux, Windows, Mac OS X) sur la machine locale

Créé un dossier pour recevoir le dépôt

    mkdir repos

Depuis le terminal, choisissez le dossier de destination

    cd ~/repos

Cloner le dépôt localement

    git clone https://NeoVox@bitbucket.org/NeoVox/jeunes-reporters-olympiques.git

Naviguer vers la racine du dossier

    cd ~/repos/projet-jeunes-reporters/

(Facultatif) Vérifier l'intégralité su dépôt

    git status

## Laravel : Poursuivre le développement

Faire une copie du fichier `.env` et renomer l'original en `.env.original`

Configuration du fichier `.env` (copié)

    DB_CONNECTION=votre système de base de données
    DB_HOST=127.0.0.1
    DB_DATABASE=votre nom de base de données
    DB_USERNAME=votre nom d'utilisateur
    DB_PASSWORD=votre mot de passe d'utilisateur

Lancer les migrations

    php artisan migrate

Lancer le serveur local

    php artisan serve

Lancer des tests unitaires

    php phpunit

Préparer l'envoi avec un message introduit par `-m`

    git commit -m 'Message'

Pour envoyer les changements vers le dépôt BitBucket, utilisez la commande `git push origin master`. Cette commande signifie que vous envoyez les changements vers la branche principale (master)

    git push origin master 

## Laravel : Configuration du serveur de production (pré-requis)

* PHP >= 5.5.9
* Extension PHP PDO
* Extension PHP OpenSSL
* Extension PHP Mbstring
* Extension PHP Tokenizer


## Laravel : Déployer un projet en production

### 1. Utilisation de la librairie "[Deployer - Deployment tool for PHP](https://deployer.org/)"

Taper ces commandes en SSH sur le serveur pour installer Deployer :

    curl -LO https://deployer.org/deployer.phar
    mv deployer.phar /usr/local/bin/dep
    chmod +x /usr/local/bin/dep

Puis dans le dossier du projet :

    dep init -t Laravel

### 2. Déploiement par connexion SSH au serveur sans librairie

Si vous disposez de git sur le serveur vous pouvez évidement l'utiliser et vous n'aurez aucun souci.

* Si vous avez Composer installé globalement sur le serveur c'est parfait.

Vous pouvez alors installer l'installeur de Laravel :

    composer global require "laravel/installer"

Il ne vous reste plus alors qu'à utiliser cet installeur

    laravel new application

* Si vous ne disposez pas de Composer installé

Dans ce cas vous commencez par envoyer l'archive `phar` de Composer.
Vous accédez à votre dossier avec SSH :

    login as: mon_login
    login@login.org's password:*****

Ensuite vous lancez la création avec Composer

    composer create-project --prefer-dist laravel/laravel

Vous attendez quelques minutes que toutes les dépendances s'installent. 

Ca devrait fonctionner mais avec un Laravel vide

Il faut ensuite envoyer le dossier `app`, le `composer.json`, le fichier `.env`, créer une base de données.

Et enfin pour récupérer toutes les dépendances du projet

    composer update

Ne pas oublier de changer les variables d'environnement pour correspondre avec le site en production

    APP_ENV=production
    APP_DEBUG=false
    APP_URL=URL du site
